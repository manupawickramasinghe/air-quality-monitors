/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   position.c
 * Author: danielq
 *
 * Created on 13 de febrero de 2020, 13:57
 */

#include "caf.h"

#define RX_BUF_SIZE 1024


static char *TAG = "position";

static SemaphoreHandle_t xSemaphore;
static float fLati;
static float fLong;
static bool fixed;

static uint8_t line[1024];
static uint8_t *ptr;
static uint8_t payload[512];
static char *tkns[100];

static const uint8_t cfgPM2[] = {0xB5, 0x62, 0x06, 0x3B};
static const uint8_t cfgPRT[] = {0xB5, 0x62, 0x06, 0x00};
static const uint8_t cfgRXM[] = {0xB5, 0x62, 0x06, 0x11};
static const uint8_t monVER[] = {0xB5, 0x62, 0x0A, 0x04};

static const uint8_t cfgRXMeco[] = {0x08, 0x04};
static const uint8_t cfgRXMpsm[] = {0x08, 0x01};

// $PUBX,00,190712.00,3435.07633,S,05829.63685,W,38.252,G3,13,12,0.705,0.00,0.089,,1.39,1.75,1.17,6,0,0*5E

static uint8_t minmeaChecksum(const char *sentence) {
    // Support senteces with or without the starting dollar sign.
    if (*sentence == '$')
        sentence++;

    uint8_t checksum = 0x00;

    // The optional checksum is an XOR of all bytes between "$" and "*".
    while (*sentence && *sentence != '*')
        checksum ^= *sentence++;

    return checksum;
}

static uint8_t *readNmeaLine(uart_port_t uart) {
    int size;
    while(1) {
        size = uart_read_bytes(uart, ptr, 1, portMAX_DELAY);
        if (size == 1) {
            if (*ptr == '\n') {
                ptr++;
                *ptr = 0;
                return line;
            }
            ptr++;
        } // End of read a character
    } // End of loop
} // End of readLine

static uint8_t *readUbxLine(uart_port_t uart) {
    int size;
    int state = 0;
    uint8_t class = 0;
    uint8_t id = 0;
    int length = 0;
    int plIdx = 0;
    uint8_t rck[2] = {0, 0};
    int off, i;
    while(1) {
        size = uart_read_bytes(uart, ptr, 1, portMAX_DELAY);
        if (size == 1) {
            switch(state) {
                case 0:
                    if(*ptr == 0x62) {
                        state++;
                    } else {
                        ptr++;
                        return readNmeaLine(uart);
                    }
                    break;
                case 1:
                    class = *ptr;
                    state++;
                    break;
                case 2:
                    id = *ptr;
                    state++;
                    break;
                case 3:
                    length = *ptr;
                    state++;
                    break;
                case 4:
                    plIdx = *ptr;
                    plIdx <<= 8;
                    length += plIdx;
                    plIdx = 0;
                    state++;
                    break;
                case 5:
                    payload[plIdx] = *ptr;
                    plIdx++;
                    if(plIdx == length) {
                        state++;
                    }
                    break;
                case 6:
                    rck[0] = *ptr;
                    state++;
                    break;
                case 7:
                    rck[1] = *ptr;
                    snprintf((char *) line, sizeof(line), "UBX class %X, id %X, length %d, ck_a %X, ck_b %X, data ", class, id, length, rck[0], rck[1]);
                    off = strlen((char *) line);
                    for(i = 0; i < length; i++) {
                        uint8_t b = payload[i] >> 4;
                        line[off++] = b > 9 ? b + 55 : b + 48;
                        b = payload[i] & 0x0F;
                        line[off++] = b > 9 ? b + 55 : b + 48;
                        line[off++] = 32;
                    }
                    line[off] = 0;
                    return line;
            }
            ptr++;
        }
    }
}

static char *readLine(uart_port_t uart) {
    int size;
    ptr = line;
    while(1) {
        size = uart_read_bytes(uart, ptr, 1, portMAX_DELAY);
        if (size == 1) {
            ptr++;
            *ptr = 0;
            if(line[0] == 0xB5) {
                return (char *) readUbxLine(uart);
            } else {
                return (char *) readNmeaLine(uart);
            }
        }
    }
}

static void writeCmdLine(const char * cmd) {
    uint8_t cs = minmeaChecksum(cmd);
    char ccs[5];
    ccs[0] = '*';
    ccs[1] = cs >> 4;
    ccs[1] += ccs[1] < 10 ? 48 : 55;
    ccs[2] = cs & 0x0f;
    ccs[2] += ccs[2] < 10 ? 48 : 55;
    ccs[3] = 13;
    ccs[4] = 10;
    ESP_LOGI(TAG, "Write cmd %s", cmd);
    ESP_LOG_BUFFER_HEXDUMP(TAG, ccs, 5, ESP_LOG_INFO);
    uart_write_bytes(GPS_UART_NUM, cmd, strlen(cmd));
    uart_write_bytes(GPS_UART_NUM, ccs, 5);
}

static void writeBinCmd(const uint8_t * cmd, const int clen, const uint8_t * data, const int dlen) {
    uint8_t ck[2] = {0, 0};
    int i;
    for(i = 2; i < clen; i++) {
        ck[0] += cmd[i];
        ck[1] += ck[0];
    }
    ESP_LOGI(TAG, "Write bin cmd");
    uart_write_bytes(GPS_UART_NUM, (const char *) cmd, clen);
    ESP_LOG_BUFFER_HEXDUMP(TAG, cmd, clen, ESP_LOG_INFO);
    uint8_t ll[2];
    ll[0] = dlen & 0xFF;
    ll[1] = dlen >> 8;
    for(i = 0; i < 2; i++) {
        ck[0] += ll[i];
        ck[1] += ck[0];
    }
    uart_write_bytes(GPS_UART_NUM, (const char *) ll, 2);
    ESP_LOG_BUFFER_HEXDUMP(TAG, ll, 2, ESP_LOG_INFO);
    if(dlen > 0) {
        for(i = 0; i < dlen; i++) {
            ck[0] += data[i];
            ck[1] += ck[0];
        }
        uart_write_bytes(GPS_UART_NUM, (const char *) data, dlen);
        ESP_LOG_BUFFER_HEXDUMP(TAG, data, dlen, ESP_LOG_INFO);
    }
    uart_write_bytes(GPS_UART_NUM, (const char *) ck, 2);
    ESP_LOG_BUFFER_HEXDUMP(TAG, ck, 2, ESP_LOG_INFO);
}

static void rxTask(void* param) {
    while (1) {
        char *line = readLine(GPS_UART_NUM);
        if(! strncmp(line, "$PUBX,00", 8)) {
            ESP_LOGI(TAG, "Read line %s", line);
        }
        if(line[0] == '$') {
            // mensaje NMEA
            uint8_t rcs = minmeaChecksum(line);
            int l = strlen(line);
            int t = 0;
            tkns[t++] = line;
            int li = 0;
            for(int i = 0; i < l; i++) {
                if(line[i] == ',' || line[i] == '*') {
                    line[i] = 0;
                    li = i + 1;
                    tkns[t++] = line + li;
                }
            }
            if(t > 3) {
                if(! strcmp(tkns[0], "$PUBX")) {
                    uint8_t ccs = (tkns[t - 1][0] > '9' ? tkns[t - 1][0] - 55 : tkns[t - 1][0] - 48) << 4;
                    ccs |= (tkns[t - 1][1] > '9' ? tkns[t - 1][1] - 55 : tkns[t - 1][1] - 48) & 0xF;
                    if(rcs == ccs) {
                        if(! strcmp(tkns[1], "00") && t == 22) {
                            // Lat/Long Position Data
                            if(! strcmp(tkns[8], "NF")) {
                                xSemaphoreTake(xSemaphore, portMAX_DELAY);
                                fixed = false;
                                xSemaphoreGive(xSemaphore);
                            } else {
                                char * rmcBuf = tkns[3];
                                float lat = (rmcBuf[2] - 48) * 10.0
                                            + (rmcBuf[3] - 48)
                                            + (rmcBuf[5] - 48) / 10.0
                                            + (rmcBuf[6] - 48) / 100.0
                                            + (rmcBuf[7] - 48) / 1000.0
                                            + (rmcBuf[8] - 48) / 10000.0
                                            + (rmcBuf[9] - 48) / 1000000.0;
                                lat /= 60.0;
                                lat += (rmcBuf[0] - 48) * 10.0
                                        + (rmcBuf[1] - 48);
                                if(tkns[4][0] == 'S') {
                                    lat = -lat;
                                }
                                rmcBuf = tkns[5];
                                float lon = (rmcBuf[3] - 48) * 10.0
                                            + (rmcBuf[4] - 48)
                                            + (rmcBuf[6] - 48) / 10.0
                                            + (rmcBuf[7] - 48) / 100.0
                                            + (rmcBuf[8] - 48) / 1000.0
                                            + (rmcBuf[9] - 48) / 10000.0
                                            + (rmcBuf[10] - 48) / 1000000.0;
                                lon /= 60.0;
                                lon += (rmcBuf[0] - 48) * 100.0
                                        + (rmcBuf[1] - 48) * 10.0
                                        + (rmcBuf[2] - 48);
                                if(tkns[6][0] == 'W') {
                                    lon = -lon;
                                }
                                xSemaphoreTake(xSemaphore, portMAX_DELAY);
                                fixed = true;
                                fLati = lat;
                                fLong = lon;
                                xSemaphoreGive(xSemaphore);
                                ESP_LOGI(TAG, "Position latitude %f, longitude %f", lat, lon);
                            }
                        } else if(! strcmp(tkns[1], "04") && t == 11) {
                            // Time of Day and Clock Information
                            // TODO
                        }
                    }
                }
            }
            
        }
    }
}

esp_err_t setEntryPosition(struct HbbTelemetryEntry *pHte) {
    esp_err_t ee;
    xSemaphoreTake(xSemaphore, portMAX_DELAY);
    if(fixed) {
        ee = ESP_OK;
        pHte->lat = fLati;
        pHte->lon = fLong;
    } else {
        ee = ESP_FAIL;
    }
    xSemaphoreGive(xSemaphore);
    return ee;
}

esp_err_t positionInit(void) {
    xSemaphore = xSemaphoreCreateMutex();
    fixed = false;
    
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };
    // We won't use a buffer for sending data.
    
    esp_err_t eee = uart_param_config(GPS_UART_NUM, &uart_config);
    if(eee != ESP_OK) {
        ESP_LOGE(TAG, "uart_param_config 0x%X", eee);
    } else {
        eee = uart_driver_install(GPS_UART_NUM, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
        if(eee != ESP_OK) {
            ESP_LOGE(TAG, "uart_driver_install 0x%X", eee);
        } else {
            ESP_LOGI(TAG, "GPS_UART OK!!!");
        }
    }
    
    xTaskCreate(&rxTask, "rx_task", 2048, NULL, 7, NULL);

    // uart_write_bytes(GPS_UART_NUM, pubx40, strlen(pubx40));
    vTaskDelay(200 / portTICK_PERIOD_MS);
    writeCmdLine("$PUBX,40,GSV,0,0,0,0,0,0");
    vTaskDelay(200 / portTICK_PERIOD_MS);
    writeCmdLine("$PUBX,40,RMC,0,0,0,0,0,0");
    vTaskDelay(200 / portTICK_PERIOD_MS);
    writeCmdLine("$PUBX,40,VTG,0,0,0,0,0,0");
    vTaskDelay(200 / portTICK_PERIOD_MS);
    writeCmdLine("$PUBX,40,GGA,0,0,0,0,0,0");
    vTaskDelay(200 / portTICK_PERIOD_MS);
    writeCmdLine("$PUBX,40,GSA,0,0,0,0,0,0");
    vTaskDelay(200 / portTICK_PERIOD_MS);
    writeCmdLine("$PUBX,40,GLL,0,0,0,0,0,0");


    vTaskDelay(200 / portTICK_PERIOD_MS);
    // $PUBX,41,portId,inProto,outProto,baudrate,autobauding*cs<CR><LF>
    writeCmdLine("$PUBX,41,1,3,3,9600,0");

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    writeBinCmd(cfgPRT, 4, NULL, 0);
    
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    // writeBinCmd(cfgRXM, 4, cfgRXMeco, 2);
    writeBinCmd(cfgRXM, 4, cfgRXMpsm, 2);

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    writeBinCmd(cfgRXM, 4, NULL, 0);

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    writeBinCmd(monVER, 4, NULL, 0);

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    writeBinCmd(cfgPM2, 4, NULL, 0);
    
    return ESP_OK;
}

void positionTask(void* param) {
    while (1) {
//        writeCmdLine("$PUBX,04");
//        vTaskDelay(1000 / portTICK_PERIOD_MS);
        writeCmdLine("$PUBX,00");
        vTaskDelay(30000 / portTICK_PERIOD_MS);
    }    
}