/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   ui.c
 * Author: danielq
 *
 * Created on 13 de febrero de 2020, 18:15
 */

#include "caf.h"

static const char *TAG = "ui";

static int blinks[6][2] = {
    {UI_RED_PIN, UI_RED_PIN},
    {UI_RED_PIN, UI_GREEN_PIN},
    {UI_RED_PIN, UI_RED_PIN},
    {UI_GREEN_PIN, UI_GREEN_PIN},
    {UI_GREEN_PIN, UI_BLUE_PIN},
    {UI_BLUE_PIN, UI_BLUE_PIN}    
};
static volatile int curState;
static volatile esp_err_t curError;

static void blinkTask(void* param) {
    gpio_set_level(UI_RED_PIN, 1);
    gpio_set_level(UI_GREEN_PIN, 1);
    gpio_set_level(UI_BLUE_PIN, 1);    
    curState = 0;
    while(true) {
        ESP_LOGI(TAG, "blinkTask %d 0x%X %d %d", curState, curError, blinks[curState][0], blinks[curState][1]);
        int blink = blinks[curState][0];
        gpio_set_level(blink, 0);
        vTaskDelay(250 / portTICK_PERIOD_MS);
        gpio_set_level(blink, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        blink = blinks[curState][1];
        gpio_set_level(blink, 0);
        vTaskDelay(250 / portTICK_PERIOD_MS);
        gpio_set_level(blink, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

esp_err_t uiInit(i2c_port_t port, SemaphoreHandle_t sem) {
    esp_err_t espRc;
    gpio_config_t gpio_cfg = {
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_OUTPUT,
        .pin_bit_mask = (1 << UI_RED_PIN) | (1 << UI_GREEN_PIN) | (1 << UI_BLUE_PIN),
        .pull_down_en = false,
        .pull_up_en = false
    };
    esp_err_t ee = gpio_config(&gpio_cfg);
    ESP_LOGI(TAG, "gpio_config 0x%X", ee);
    xTaskCreate(&blinkTask, "blink_task", 2048, NULL, 7, NULL);
    espRc = ESP_OK;
    return espRc;
}

esp_err_t uiShow(int state, esp_err_t error) {
    ESP_LOGI(TAG, "uiShow %d 0x%X", state, error);
    esp_err_t espRc;
    curState = state;
    curError = error;
    espRc = ESP_OK;
    return espRc;
}
