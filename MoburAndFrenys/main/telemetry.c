/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   telemetry.c
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 10:32
 */

#include "maf.h"

#include "driver/i2c.h"
#include "esp_system.h"
#include <sys/time.h>


static const char *TAG = "telemetry";
static int k;
static int state;
static struct timeval tv;
static struct timezone *tz;
static char mqttbuf[1024];
static bool haveMqtt;


void cubicClose(void) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (I2C_CUBIC_ADDRESS << 1) | I2C_MASTER_WRITE, 1);
    i2c_master_write_byte(cmd, 0x16, 1);
    i2c_master_write_byte(cmd, 0x07, 1);
    i2c_master_write_byte(cmd, 0x01, 1); // close
    i2c_master_write_byte(cmd, 0x00, 1);
    i2c_master_write_byte(cmd, 0x00, 1);
    i2c_master_write_byte(cmd, 0x00, 1);
    i2c_master_write_byte(cmd, 0x16 ^ 0x07 ^ 0x01, 1); // check code
    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    ESP_LOGI(TAG, "Close measurement %d", ret);
}

void cubicOpen(void) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (I2C_CUBIC_ADDRESS << 1) | I2C_MASTER_WRITE, 1);
    i2c_master_write_byte(cmd, 0x16, 1);
    i2c_master_write_byte(cmd, 0x07, 1);
    i2c_master_write_byte(cmd, 0x02, 1); // open
    i2c_master_write_byte(cmd, 0x00, 1);
    i2c_master_write_byte(cmd, 0x00, 1);
    i2c_master_write_byte(cmd, 0x00, 1);
    i2c_master_write_byte(cmd, 0x16 ^ 0x07 ^ 0x02, 1); // check code
    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    ESP_LOGI(TAG, "Open measurement %d", ret);
}

void cubicReadAndPublish(void) {
    uint8_t data[I2C_CUBIC_FRAME_SIZE];
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (I2C_CUBIC_ADDRESS << 1) | I2C_MASTER_READ, 1);
    i2c_master_read(cmd, data, I2C_CUBIC_FRAME_SIZE, 0);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    ESP_LOGI(TAG, "Read data %d", ret);
    if(ret == ESP_OK) {
        ESP_LOG_BUFFER_HEXDUMP(TAG, data, I2C_CUBIC_FRAME_SIZE, ESP_LOG_INFO);
    }
    // calculamos el check code
    uint8_t ccc = 0;
    for(int i = 0, l = I2C_CUBIC_FRAME_SIZE - 1; i < l; i++) {
        ccc ^= data[i];
    }
    ESP_LOGI(TAG, "rcc %X, ccc %X", data[24], ccc);
    if(ccc == data[24]) {
        // el check code calculado es igual al recibido: extraemos los datos
        int co2 = ((int) data[3]) << 8;
        co2 |= data[4];
        int voc = ((int) data[5]) << 8;
        voc |= data[6];
        int hum = ((int) data[7]) << 8;
        hum |= data[8];
        int tem = ((int) data[9]) << 8;
        tem |= data[10];
        int g10 = ((int) data[11]) << 8;
        g10 |= data[12];
        int g25 = ((int) data[13]) << 8;
        g25 |= data[14];
        int g100 = ((int) data[15]) << 8;
        g100 |= data[16];
        int t10 = ((int) data[17]) << 8;
        t10 |= data[18];
        int t25 = ((int) data[19]) << 8;
        t25 |= data[20];
        int t100 = ((int) data[21]) << 8;
        t100 |= data[22];
        ESP_LOGI(TAG, "Modo %X, estado %X, CO2 %d, VOC %d, humedad %.1f, temperatura %.1f", 
                data[2], data[23], co2, voc, hum / 10.0, (tem - 500) / 10.0);
        ESP_LOGI(TAG, "GRIMM PM1.0 %d, PM2.5 %d, PM10 %d", 
                g10, g25, g100);
        ESP_LOGI(TAG, "TSI PM1.0 %d, PM2.5 %d, PM10 %d", 
                t10, t25, t100);
        gettimeofday(&tv, &tz);
        ESP_LOGI(TAG, "time %ld.%06ld", tv.tv_sec, tv.tv_usec);
        if(tv.tv_sec > 1500000000) {
            // mandar vía mqtt
            snprintf(mqttbuf, 1024, 
                    "{\"ts\":%ld000,\"values\":{"
                    "\"lat\":%f,\"lon\":%f,"
                    "\"t\":%.1f,\"h\":%.1f,"
                    "\"co2\":%d,\"voc\":%d,"
                    "\"pm10\":%d,\"pm25\":%d,\"pm100\":%d}}",
                    (long) tv.tv_sec,
                    fLati, fLong,
                    (tem - 500) / 10.0, hum / 10.0,
                    co2, voc,
                    t10, t25, t100
                    );
            commsPublishData(mqttbuf);
        }
    }
    ESP_LOGI(TAG, "esp_get_free_heap_size %X", esp_get_free_heap_size());
}

esp_err_t telemetryInit(void) {
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.clk_stretch_tick = I2C_CLK_STRETCH_TICK;
    i2c_driver_install(i2c_master_port, conf.mode);
    i2c_param_config(i2c_master_port, &conf);
    haveMqtt = false;
    state = 0;
    k = 0;
    return ESP_OK;
}

void telemetryTask(void* param) {
    while(true) {
        EventBits_t uxBits = xEventGroupWaitBits(
                 egTelemetry,    // The event group being tested.
                 BIT0 | BIT1,    // The bits within the event group to wait for.
                 pdTRUE,         // BIT0 and BIT1 should be cleared before returning.
                 pdFALSE,        // Don't wait for both bits, either bit will do.
                 1000 / portTICK_PERIOD_MS);
        ESP_LOGI(TAG, "uxBits %X", uxBits);
        if(uxBits & BIT0) {
            haveMqtt = false;
            state = 0;
        } else if(uxBits & BIT1) {
            haveMqtt = true;
            if(state == 1) {
                state++;
            }
        } else {
            k++;
            switch(state) {
                case 0:
                    cubicClose();
                    k = 0;
                    if(haveMqtt) {
                        state += 2;
                    } else {
                        state++;
                    }
                    break;
                case 2:
                    if(k >= CUBIC_SECONDS_FOR_OPEN) {
                        cubicOpen();
                        state++;
                    }
                    break;
                case 3:
                    if(k >= CUBIC_SECONDS_FOR_READ) {
                        cubicReadAndPublish();
                        state = 0;
                    }
                    break;
            }
        }
    }
}
